# AscendGaussianBlur

### Dependency Library

Fwkacllib ---contains acllib and IR build framework

opencv 4.x ---Compiled with gcc-4.8.5) download link: 链接: https://pan.baidu.com/s/1-DMsU58Jqf85STq2kTBGJw  密码: qmhu

### Usage:

before compile, please set environment variables as follow:

```
source /usr/local/Ascend/ascend-toolkit/set_env.sh
export PATH=/usr/local/python3.7.5/bin:$PATH
export LD_LIBRARY_PATH=/usr/local/python3.7.5/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/Ascend/ascend-toolkit/latest/fwkacllib/lib64/:/root/yolaw/opencv-ascend/lib64
```

```
aclInit(nullptr);
imwrite("src.jpg", img);
AscendGaussianBlur blur(img.cols, img.rows, img.channels(),  kernel_size,  sigma);
.....
dst = blur.run(img);
imwrite("ascend_blur.jpg", dst);
aclFinalize();
```

run sample:

```
mkdir build
cmake ..
make
./AscendGaussianBlur 0 640 480 test.jpg    
#set 0 to use Ascend310, set 1 to use opencv with single cpu,set  >1 to use opencv with mutil cpu

```



#### Pay Attention

the opencv lib must compiled with gcc-4.8.5 and the `_GLIBCXX_USE_CXX11_ABI=0` must be write in CMakeLists.txt. 

### Result:

img size: 640x360

![](./src.jpg)



kernel_size:13, sigma: 2.5

![](./ascend_blur.jpg)



