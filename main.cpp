#define _GNU_SOURCE
#include <sched.h>
#include <iostream>
#include "AscendGaussianBlur.h"
#include <sys/time.h>
int main(int argc, char** argv) {
    //set opencv cpu limit
    cpu_set_t mask;
    CPU_ZERO(&mask);
    for(int i=0; i < atoi(argv[1]); i++){
        CPU_SET(i,&mask); 
    }  
    sched_setaffinity(0, sizeof(cpu_set_t), &mask);

    struct timeval start;
    struct timeval end;
    Mat img = imread(argv[4]);
    resize(img, img, Size(atoi(argv[2]),atoi(argv[3])));
    std::cout << img.cols <<" "<< img.rows << std::endl;
    imwrite("src.jpg", img);
    aclInit(nullptr);
    AscendGaussianBlur blur(img.cols, img.rows, 3, 13, 2.5);

    Mat dst;
    Mat dst_1;
    switch(atoi(argv[1])){
        case 0:

            gettimeofday(&start,NULL);
            dst = blur.run(img);
            gettimeofday(&end,NULL);
            cout<<"ascend blur time : "<<(end.tv_sec-start.tv_sec)*1000+(end.tv_usec-start.tv_usec) / 1000.0 <<"ms"<<endl;
            imwrite("ascend_blur.jpg", dst);
            break;
        default:
            gettimeofday(&start,NULL);
            GaussianBlur(img, dst_1, Size(13,13), 2.5,2.5);
            gettimeofday(&end,NULL);
            cout<<"opencv blur time : "<<(end.tv_sec-start.tv_sec)*1000+(end.tv_usec-start.tv_usec) / 1000.0 <<"ms"<<endl;
            imwrite("opencv_blur.jpg", dst_1);
            break;
    }
    aclFinalize();
    return 0;
}
