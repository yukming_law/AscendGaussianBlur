//
// Created by yo on 21-5-29.
//

#ifndef ASCEND_GAUSSIANBLUR_ASCENDGAUSSIANBLUR_H
#define ASCEND_GAUSSIANBLUR_ASCENDGAUSSIANBLUR_H

#include "all_ops.h"
#include "graph.h"
#include "types.h"
#include "tensor.h"
#include "attr_value.h"
#include "ascend_string.h"
#include "opencv2/opencv.hpp"
#include "ge_api_types.h"
#include "ge_ir_build.h"
#include "ModelProcess.h"
using namespace std;
using namespace cv;
using namespace ge;
using ge::Operator;

class AscendGaussianBlur {
public:
    AscendGaussianBlur(int src_w, int src_h, int _channel, int kernel_size, double sigma);
    ~AscendGaussianBlur();
    Mat run(Mat& img);
private:
    int src_h;
    int src_w;
    int channel;
    std::map<AscendString, AscendString> options;
    Graph graph;
    aclrtContext m_context;
    aclrtStream m_stream;
    shared_ptr<ModelProcess> m_modelProcess;
    aclmdlDesc *m_modelDesc;
    std::vector<void *> inputBuffers;
    std::vector<size_t> inputSizes;
    std::vector<void *> outputBuffers;
    std::vector<size_t> outputSizes;
};


#endif //ASCEND_GAUSSIANBLUR_ASCENDGAUSSIANBLUR_H
